package com.equipo2.Appkademy.core.service;

public interface NotificationService {

    void sendEmailNotification(String fullName, String email);

}
